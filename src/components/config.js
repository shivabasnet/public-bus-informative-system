import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyCpvaaNfjYp-ON8axiAM7PIEvsIIl4tMf8",
  authDomain: "public-bus-informative-system.firebaseapp.com",
  databaseURL: "https://public-bus-informative-system.firebaseio.com",
  projectId: "public-bus-informative-system",
  storageBucket: "public-bus-informative-system.appspot.com",
  messagingSenderId: "92579529149",
  appId: "1:92579529149:web:8aa484aaae40ba64c852d4",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebaseConfig;
