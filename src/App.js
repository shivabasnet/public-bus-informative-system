import React from "react";
import logo from "./logo.svg";
import "./App.css";
import config from "./components/config";
import firebase from "firebase";

class App extends React.Component {
  constructor(props) {
    super(props);
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }
    this.database = firebase.database().ref();
    this.state = {
      IsBusAtStation_1: false,
      IsBusAtStation_2: false,
      seat_status: "Available",
    };
  }
  componentDidMount() {
    this.database.on("value", (snap) => {
      console.log(snap.val().IsBusAtStation_1);
      this.setState({
        IsBusAtStation_1: snap.val().IsBusAtStation_1,
        IsBusAtStation_2: snap.val().IsBusAtStation_2,
        seat_status: snap.val().seat_status,
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.IsBusAtStation_1 ? <h1>"True"</h1> : <h1>"False"</h1>}
        {this.state.IsBusAtStation_2 ? <h1>"True"</h1> : <h1>"False"</h1>}
        <h2>{this.state.seat_status}</h2>
      </div>
    );
  }
}
export default App;
